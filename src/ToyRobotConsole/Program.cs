﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace ToyRobotConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var services = Startup.ConfigureServices().BuildServiceProvider();
            services.GetService<App>().Run();
        }
    }
}
