﻿using Application;
using Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ToyRobotConsole
{
    public class App
    {
        private readonly ICommandFactory _commandFactory;
        public App(ICommandFactory commandFactory)
        {
            _commandFactory = commandFactory ?? throw new ArgumentNullException(nameof(_commandFactory));
        }

        public void Run()
        {
            bool continueRun = true;

            WriteHelpInfo();

            while (continueRun)
            {
                var commandString = Console.ReadLine().Trim();
                if (string.Compare(commandString, "exit", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    break;
                }
                
                var command = _commandFactory.GetCommand(commandString);

                if (command == null)
                {
                    // currently there is no out put for wrong command
                    continue;
                }

                var result = command.ProcessCommand(commandString);

                // could put more complex logic here to control the output
                if (result.Status == Application.Enums.ResponseStatus.Success
                    && !string.IsNullOrWhiteSpace(result.Message))
                {
                    Console.WriteLine(result.Message);
                }
            }
        }

        private void WriteHelpInfo()
        {
            
            Console.WriteLine("Supported commands:");
            Console.WriteLine("PLACE X, Y, DIRECTIONPLACE");
            Console.WriteLine("MOVE");
            Console.WriteLine("LEFT");
            Console.WriteLine("RIGHT");
            Console.WriteLine("REPORT");
            Console.WriteLine("RESET");
            Console.WriteLine("EXIT");
            Console.WriteLine("Please input command line");
        }
    }
}
