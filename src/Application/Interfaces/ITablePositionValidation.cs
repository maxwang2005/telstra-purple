﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public interface ITablePositionValidation
    {
        bool IsPositionValid(Position position);
    }
}
