﻿using Application.Entities;
using Application.Enums;
using System;

namespace Application
{
    public interface ICommand
    {
        string CommandValidationString { get; }

        CommandType CommandType { get; }

        CommandResponse ProcessCommand(string command);
    }
}