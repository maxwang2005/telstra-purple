﻿using Application.Entities;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public interface ITableService
    {
        /// <summary>
        /// Gets table robot 
        /// </summary>
        /// <returns>
        ///   <br />
        /// </returns>
        ToyRobot GetTableRobot();

        void Reset();
        
    }
}
