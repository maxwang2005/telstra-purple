﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Interfaces
{
    public interface ICommandFactory
    {
        ICommand GetCommand(string command);
    }
}
