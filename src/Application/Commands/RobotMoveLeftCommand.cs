﻿using Application.Entities;
using Application.Enums;
using Application.Interfaces;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands
{
    public class RobotMoveLeftCommand : RobotMoveBaseCommand
    {
        public RobotMoveLeftCommand(
            ITableService tableService,
            ITablePositionValidation tablePositionValidation)
            : base(tableService, tablePositionValidation)
        {
        }

        public override string CommandValidationString => "LEFT";

        public override CommandType CommandType => CommandType.RobotCommand;

        protected override Domain.Entities.Position GetNextPosition(int x, int y, FacingDirection direction)
        {
            
            switch (direction)
            {
                case FacingDirection.EAST:
                    direction = FacingDirection.NORTH;
                    break;
                case FacingDirection.WEST:
                    direction = FacingDirection.SOUTH;
                    break;
                case FacingDirection.NORTH:
                    direction = FacingDirection.WEST;
                    break;
                case FacingDirection.SOUTH:
                    direction = FacingDirection.EAST;
                    break;
            }

            return new Position
            {
                X = x,
                Y = y,
                Direction = direction
            };
        }
    }
}
