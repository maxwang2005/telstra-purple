﻿using Application.Entities;
using Application.Enums;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Application.Commands
{
    public class RobotPlaceCommand : ICommand
    {
        private readonly ITableService _table;
        private readonly ITablePositionValidation _positionValidator;
        public RobotPlaceCommand(
            ITableService tableService,
            ITablePositionValidation positionValidator)
        {
            _table = tableService ?? throw new ArgumentNullException(nameof(tableService));
            _positionValidator = positionValidator ?? throw new ArgumentNullException(nameof(positionValidator));
        }

        public string CommandValidationString => @"PLACE\s+(\d)+\s*,\s*(\d+)\s*$";

        public CommandType CommandType => CommandType.RobotCommand;

        public CommandResponse ProcessCommand(string command)
        {
            var matches = Regex.Matches(command, CommandValidationString, RegexOptions.IgnoreCase);
            if (matches == null || matches.Count != 1)
            {
                return new CommandResponse
                {
                    Status = ResponseStatus.Fail,
                    Message = "Command not match"
                };
            }

            var matchGroups = matches[0].Groups;

            var robot = _table.GetTableRobot();

            if (!robot.IsPlacedOnTable)
            {
                return new CommandResponse
                {
                    Status = ResponseStatus.Ignore
                };
            }

            var nextPostion = new Position
            {
                X = int.Parse(matchGroups[1].Value),
                Y = int.Parse(matchGroups[2].Value),
                Direction = robot.Position.Direction
            };

            if (!_positionValidator.IsPositionValid(nextPostion))
            {
                return new CommandResponse
                {
                    Status = ResponseStatus.Ignore,
                    Message = "Blocked"
                };
            }

            robot.Position = nextPostion;

            return new CommandResponse
            {
                Status = ResponseStatus.Success
            };

        }
    }
}
