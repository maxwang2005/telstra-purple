﻿using Application.Entities;
using Application.Enums;
using Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands
{
    public class RobotReportCommand : ICommand
    {
        private readonly ITableService _table;
        public RobotReportCommand(
            ITableService tableService,
            ITablePositionValidation tablePositionValidation)
        {
            _table = tableService ?? throw new ArgumentNullException(nameof(tableService));
        }
        
        public string CommandValidationString => "REPORT";

        public CommandType CommandType => CommandType.RobotCommand;

        public CommandResponse ProcessCommand(string command)
        {
            var robot = _table.GetTableRobot();

            if (robot == null)
            {
                return new CommandResponse
                {
                    Status = ResponseStatus.Ignore
                };
            }

            if (!robot.IsPlacedOnTable)
            {
                return new CommandResponse
                {
                    Status = ResponseStatus.Ignore
                };
            }

            return new CommandResponse
            {
                Status = ResponseStatus.Success,
                Message = robot.ToString()
            };
        }
    }
}
