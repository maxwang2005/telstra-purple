﻿using Application.Entities;
using Application.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Commands
{
    public class ResetCommand : ICommand
    {
        private readonly ITableService _table;
        public ResetCommand(ITableService tableService)
        {
            _table = tableService ?? throw new ArgumentNullException(nameof(tableService));
        }

        public string CommandValidationString => "RESET";

        public CommandType CommandType => CommandType.TableCommand;

        public CommandResponse ProcessCommand(string command)
        {
            _table.Reset();

            return new CommandResponse
            {
                Status = ResponseStatus.Success
            };
        }
    }
}
