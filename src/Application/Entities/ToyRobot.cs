﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Entities
{
    public class ToyRobot : Robot
    {
        public bool IsPlacedOnTable { get; set; }
    }
}
