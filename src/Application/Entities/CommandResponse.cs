﻿using Application.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.Entities
{
    public class CommandResponse
    {
        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
    }
}
