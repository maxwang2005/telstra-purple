﻿
using Application.Entities;
using Application.Interfaces;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Application.Services
{
    public class TableService : ITableService, ITablePositionValidation
    {
        private SquareTable _table;
        private ToyRobot _robot;
        public TableService()
        {
            _table = new SquareTable(6);
            // Currently we do not support multiple robot on table
            _robot = new ToyRobot();
        }

        public ToyRobot GetTableRobot()
        {
            return _robot;
        }

        public bool IsPositionValid(Position position)
        {
            if (position == null)
            {
                return false;
            }

            return position.X >= 0 &&
                position.Y >= 0 &&
                position.X < _table.Width - 1 &&
                position.Y < _table.Length - 1;
        }

        public void Reset()
        {
            _robot.IsPlacedOnTable = false;
            _robot.Position = null;
        }

    }
}