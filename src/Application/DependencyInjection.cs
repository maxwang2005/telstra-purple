﻿using Application.Commands;
using Application.Interfaces;
using Application.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Inject all application level services
        /// </summary>
        /// <param name="services">The services.</param>
        /// <returns>
        ///   <br />
        /// </returns>
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddScoped<ITableService, TableService>();
            services.AddScoped<ITablePositionValidation, TableService>();
                        
            // add commands
            services.AddScoped<ICommand, PlaceRobotOnTableCommand>();
            services.AddScoped<ICommand, RobotMoveLeftCommand>();
            services.AddScoped<ICommand, RobotMoveRightCommand>();
            services.AddScoped<ICommand, RobotPlaceCommand>();
            services.AddScoped<ICommand, RobotMoveCommand>();
            services.AddScoped<ICommand, RobotReportCommand>();
            services.AddScoped<ICommand, ResetCommand>();

            services.AddScoped<ICommandFactory, CommandFactory>();
            return services;
        }
    }
}
