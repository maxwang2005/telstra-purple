﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum FacingDirection
    {
        NORTH,
        EAST,
        SOUTH,
        WEST
    }
}
