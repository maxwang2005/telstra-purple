﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class SquareTable
    {
        public SquareTable(int width)
        {
            Width = width;
        }

        public int Width { get; set; }
        public int Length => Width;
    }
}
