﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Domain.Entities
{
    public class Robot
    {

        public Position Position { get; set; }

        public override string ToString()
        {
            return Position == default
                ? string.Empty
                : $"{Position.X},{Position.Y},{Position.Direction}";
        }
    }
}
