﻿using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Entities
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public FacingDirection Direction { get; set; }
    }
}
