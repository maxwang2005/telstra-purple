# Telstra Purple coding exercise

This is Max Wang's coding exercise for Telstra Purple. Create a library that can read in commands of the following form:
PLACE X, Y, DIRECTION
MOVE
LEFT
RIGHT
REPORT

## Technical Decision

- C#
- .NET Core SDK 3.1
- Visual Studio 2019
- xUnit
- Moq

**Note:**
I use .net core 3.1 instead of 5.0, just because .net core 3.1 is a long-time supported version. The project could be converted to .net core 5.0 if needed. .Net core 6 is very new, but could migrate to .net core 6 if needed.

## This solution is based on the below assumptions:

- Only allow on toy robot on the table
- Do not need create the table (This can be done with new command if needed)
- Do not need save toy robot information.
- Do not add the cache, but could add this if needed.
- No log needed. But could add NLog or Serilog with lines of code

## Folder Description

- `src` project source code
- `tests` all tests
- `docs` origin coding exercise document

### src

- `ToyRobotConsole` main project
- `Application`
- `Infrastructure`
- `Domain`

## How to run the program

### run with Visual Studio

You could start the project with Visual Studio and `Run` or `Debug` the **ToyRobotConsole** project
