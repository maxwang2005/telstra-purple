﻿using Application.Entities;
using Domain.Entities;

namespace Application.UnitTests.Entities
{
    public class RobotMoveTestCase
    {
        public Position CurrentLocation { get; set; }
        public Position ExpectedNextLocation { get; set; }
        public CommandResponse ExpectedReponse { get; set; }
        public bool IsPlacedOnTable { get; set; }
        public int? TableWidth { get; set; }
        public string TestName { get; set; }

        public override string ToString()
        {
            return TestName;
        }
    }
}