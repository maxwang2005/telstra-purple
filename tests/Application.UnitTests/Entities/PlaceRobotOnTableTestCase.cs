﻿using Application.Entities;

namespace Application.UnitTests.Entities
{
    public class PlaceRobotOnTableTestCase
    {
        public CommandResponse ExpectedReponse { get; set; }
        public string InputCommand { get; set; }
        public bool IsPlacedOnTable { get; set; }
        public int? TableWidth { get; set; }
        public string TestName { get; set; }

        public override string ToString()
        {
            return TestName;
        }
    }
}