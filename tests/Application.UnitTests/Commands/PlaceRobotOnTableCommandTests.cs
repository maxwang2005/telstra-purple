﻿using Application.Commands;
using Application.Entities;
using Application.Enums;
using Application.UnitTests.Entities;
using Application.UnitTests.TestCases;
using Domain.Entities;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Application.UnitTests.Commands
{
    public class PlaceRobotOnTableCommandTests
    {
        private readonly Mock<ITableService> _mockTable;
        private readonly Mock<ITablePositionValidation> _mockValidation;
        public PlaceRobotOnTableCommandTests()
        {
            _mockTable = new Mock<ITableService>();
            _mockValidation = new Mock<ITablePositionValidation>();
        }


        /// <summary>
        /// This test focus on command parsing
        /// 
        /// </summary>
        /// <param name="commandString">The command string.</param>
        [Theory]
        [InlineData("PLACE 1,1,EAST")]
        [InlineData("PLACE 1,1, EAST")]
        [InlineData("PLACE 1,1 ,EAST")]
        [InlineData("place 1,1,east")]
        [InlineData("PLACE 1, 1,EAST")]
        [InlineData("PLACE 1, 1 ,EAST")]
        [InlineData("PLACE \t1,1,EAST")]
        [InlineData("PLACE 1, 1\t,EAST")]
        [InlineData("PLACE 1 , 1,EAST")]
        [InlineData("PLACE 1,1,EAST ")]
        public void PlaceRobotOnTableCommand_Should_Work_When_There_is_Space_in_Command(string commandString)
        {
            /// Arrange
            _mockValidation.Setup(x => x.IsPositionValid(It.IsAny<Position>())).Returns(true);
            _mockTable.Setup(x => x.GetTableRobot()).Returns(new ToyRobot
            {
                IsPlacedOnTable = false
            });
            var command = new PlaceRobotOnTableCommand(_mockTable.Object, _mockValidation.Object);

            /// Act
            var result = command.ProcessCommand(commandString);

            /// Assert
            result.Should().Equals(new CommandResponse
            {
                Status = ResponseStatus.Success
            });
        }



        /// <summary>
        /// Places the robot on table command business logic
        /// </summary>
        /// <param name="data">The data.</param>
        [Theory]
        [ClassData(typeof(PlaceRobotOnTableTestCases))]
        public void PlaceRobotOnTableCommand_Should_Work_For_Different_Input(PlaceRobotOnTableTestCase data)
        {
            /// Arrange
            _mockTable.Setup(x => x.GetTableRobot()).Returns(new ToyRobot
            {
                IsPlacedOnTable = data.IsPlacedOnTable
            });

            if (data.TableWidth.HasValue)
            {
                _mockValidation.Setup(x => x.IsPositionValid(It.IsAny<Position>())).Returns((Position x) => x.X >= 0
                    && x.Y >= 0
                    && x.X < data.TableWidth
                    && x.Y < data.TableWidth);
            }

            _mockTable.Setup(x => x.GetTableRobot()).Returns(new ToyRobot
            {
                IsPlacedOnTable = false
            });
            var command = new PlaceRobotOnTableCommand(_mockTable.Object, _mockValidation.Object);

            /// Act
            var result = command.ProcessCommand(data.InputCommand);

            /// Assert
            result.Should().BeEquivalentTo(data.ExpectedReponse);
        }

    }
}
