﻿using Application.Commands;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.UnitTests.MoqObject
{
    public class RobotMoveRightCommandMoq : RobotMoveRightCommand
    {
        public RobotMoveRightCommandMoq(ITableService tableService, ITablePositionValidation tablePositionValidation) : base(tableService, tablePositionValidation)
        {
        }

        // this help us to test the protection method
        public Position GetProtectedNextPosition(int x, int y, FacingDirection direction)
        {
            return GetNextPosition(x, y, direction);
        }
    }
}
