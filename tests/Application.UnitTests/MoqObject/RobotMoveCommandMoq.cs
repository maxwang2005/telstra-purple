﻿using Application.Commands;
using Domain.Entities;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.UnitTests.MoqObject
{
    public class RobotMoveCommandMoq : RobotMoveCommand
    {
        public RobotMoveCommandMoq(ITableService tableService, ITablePositionValidation tablePositionValidation) : base(tableService, tablePositionValidation)
        {
        }

        public Position GetProtectedNextPosition(int x, int y, FacingDirection direction)
        {
            return GetNextPosition(x, y, direction);
        }
    }
}
