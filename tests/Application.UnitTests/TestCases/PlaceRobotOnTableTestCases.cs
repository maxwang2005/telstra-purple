﻿using Application.Entities;
using Application.Enums;
using Application.UnitTests.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Application.UnitTests.TestCases
{
    class PlaceRobotOnTableTestCases : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            yield return GetNotMatchedTestCase();
           
        }

        private object[] GetNotMatchedTestCase()
        {
            return new object[]
           {
                new PlaceRobotOnTableTestCase
                {
                    InputCommand = "this is a test",
                    IsPlacedOnTable = false,
                    TableWidth = 5,
                    ExpectedReponse = new CommandResponse
                    {
                        Status = ResponseStatus.Fail,
                        Message = "Command not match"
                    },
                    TestName = "Not Matched command should fail"
                }
           };
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
